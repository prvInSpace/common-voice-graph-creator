#!/usr/bin/env python3

import json
import re
from datetime import datetime
from pathlib import Path
from typing import Any

import numpy as np
import pandas as pd

# Used so that it can be found regardless of where the
# script is called from
COMMON_VOICE_PATH = Path(__file__).parent.parent.parent / "cv-dataset" / "datasets"
DATA_COLUMNS = ["totalHrs", "validHrs", "train", "dev", "test", "validated"]


def load_common_voice_data() -> dict[str, pd.DataFrame]:
    """Loads all of the data from the common voice folder
    for all of the languages. This data is then compiled into a
    DataFrame for each language and added to a dictionary.

    Returns:
        dict[str, pd.DataFrame]: A dictionary with languages and the data for those languages
    """

    langs: dict[str, pd.DataFrame] = {}
    for file in COMMON_VOICE_PATH.glob("*.json"):
        # Filter out some of the files we don't care about
        if "singleword" in file.name or "delta" in file.name:
            continue

        with file.open() as json_file:
            obj = json.load(json_file)

            # Get the date from the field if it exist,
            # else extract it from the file name
            if "date" in obj:
                date = obj["date"]
            else:
                date = re.search("20[0-9]{2}-[0-9]{2}-[0-9]{2}", file.name).group()

            # Fetch the version from the file name
            version = re.search(r"(?<=corpus-)[0-9]+(?:\.[0-9])?", file.name).group()

            # Extract data for each language
            for lang in obj["locales"]:
                data = obj["locales"][lang]

                if lang not in langs:
                    langs[lang] = pd.DataFrame(
                        columns=["Version", "Date", *DATA_COLUMNS]
                    )

                # CV 1, 2, 3 doesn't have these values, so leave them at NaN
                # if not found.
                totalHrs = data["totalHrs"] if "totalHrs" in data else np.nan
                validHrs = data["validHrs"] if "validHrs" in data else np.nan
                buckets: dict[str, Any] = data["buckets"]

                # Add a new row to the dataset
                langs[lang].loc[len(langs[lang])] = [
                    version,
                    datetime.strptime(date, "%Y-%m-%d"),
                    float(totalHrs),
                    float(validHrs),
                    # Sets can be missing if empty
                    buckets.get("train", 0),
                    buckets.get("dev", 0),
                    buckets.get("test", 0),
                    buckets.get("validated", 0),
                ]

    # Calculate the utilization for each language
    for lang, df in langs.items():
        # Validated can be empty, so make it at least 1 to prevent division by 0.
        langs[lang]["utilization"] = df.apply(
            lambda row: (row["train"] + row["dev"] + row["test"])
            / max(1, row["validated"]),
            axis=1,
        )

    return langs
