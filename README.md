# Common Voice Graph Creator

A small script that can be used to graph data from the Common Voice datasets.

## Initialise repository
In order to run the script you have to initialise the submodules. To do this, run the following command:
```bash
git submodule update --init --recursive
```

## Usage

While there are many ways to run the script, there are two main ones:

### Using uv
If you, like me have joined the uv hypetrain, you can run the main script by running the following command: 
```bash
uv run cv-graph-creator <flags>
```

### Using regular Python

Otherwise, assuming that you have set up a virtual environment, you can use the following command:
```bash
python3 -m src.cv_graph_creator <flags>
```

### Defaults etc.
Several languages and variables can be specified using the `--langs` and `--vars` flags.

If only one language and and several variables are specified, the lines will be named after the variable. For example `python3 scripts/graph_cv.py --langs cy --vars train dev test`
looks something like:
![Welsh datasets](examples/cy-datasets.png)

If multiple languages but only one variable are specified, the lines will be named after the language. For example `python3 scripts/graph_cv.py --langs br cy ga-IE --vars totalHrs`
looks something like:
![Total hours for 3 Celtic languages](examples/celtic-totalhrs.png)

Finally, if multiple languages and multiple variables are specified, the lines will be named lang:variable for each language and variable pair in the output.

In addition to this, a title and a y-label (other than the automatically generated ones) can be assigned using the `--title` and the `--ylabel` flags.

## Maintainer

* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;