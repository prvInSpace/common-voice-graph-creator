#!/usr/bin/env python3

from src.cv_graph_creator.cv_data import DATA_COLUMNS, load_common_voice_data
import pandas as pd
import matplotlib.pyplot as plt
import argparse


def fetch_args():
    parser = argparse.ArgumentParser(
        description="Graphs data from Common Voice.", exit_on_error=True
    )
    parser.add_argument("--langs", nargs="+", required=True)
    parser.add_argument(
        "--vars", nargs="+", choices=[*DATA_COLUMNS, "utilization"], required=False
    )
    parser.add_argument(
        "--by-version",
        action="store_true",
        help="Uses version instead of date for the x-axis.",
    )
    parser.add_argument("--title", nargs="?", help="Adds a title to the graph")
    parser.add_argument("--ylabel", nargs="?", help="Specifies a y-label")
    parser.add_argument("--output", help="The file to write the graph to")
    return parser.parse_args()


def main():
    args = fetch_args()
    data = load_common_voice_data()

    df = pd.DataFrame()
    df["Version"] = data["en"]["Version"]
    df = df.merge(data["en"][["Version", "Date"]], on="Version", how="left")

    for lang in args.langs:
        if lang not in data:
            print(
                f"Error: Unknown language code {lang!r}. Found: {', '.join(sorted(data.keys()))}"
            )
            exit(10)

        new = data[lang][["Version", *args.vars]]
        # print(new)
        df = df.merge(new, on="Version", how="left")
        if len(args.vars) > 1 and len(args.langs) > 1:
            for arg in args.vars:
                df.rename(columns={arg: f"{lang}:{arg}"}, inplace=True)
        elif len(args.langs) > 1:
            df.rename(columns={args.vars[0]: lang}, inplace=True)

    df.sort_values(by=["Date", "Version"], inplace=True)
    print(df)

    # Rename columns to more friendly names if possible
    if len(args.langs) == 1:
        df.rename(
            columns={"totalHrs": "Total hours", "validHrs": "Valid hours"}, inplace=True
        )

    if args.by_version:
        del df["Date"]
        df.plot(x="Version")
    else:
        df.plot(x="Date")

    # Add Y-label if all variables are the same metric
    if len(set(["totalHrs", "validHrs"]).intersection(args.vars)) == len(args.vars):
        plt.ylabel("Hours")

    if len(set(["train", "dev", "test"]).intersection(args.vars)) == len(args.vars):
        plt.ylabel("Recordings")

    if "utilization" in args.vars and len(args.vars) == 1:
        import matplotlib.ticker as mtick

        plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1.0))
        plt.ylabel("% of validated utilized")

    if args.ylabel:
        plt.ylabel(args.ylabel)
    if args.title:
        plt.title(args.title)

    if args.output:
        plt.savefig(args.output)
    plt.show()


if __name__ == "__main__":
    main()
